from window import *

def main():
    # create a new window with the specified dimensions
    root = Tk()
    root.geometry('800x600')
    gui_window = Window(root)

    # keeps the window alive...
    gui_window.mainloop()
main()