import tkinter as tkinter
from tkinter import *
from tkinter.font import Font
from tkinter import messagebox
import sqlite3

""" This class creates a GUI window for an application (inherits from Frame) """
class Window(Frame):

    firstname_entry = None
    firstname_field = None
    lastname_entry = None
    lastname_field = None

    """ Initialize the new window """
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.master = master  
        self.title_font = Font(family='Helvetica', size=20, weight='bold', slant='italic')   
        self.populate_window()

    """ Populate the window with some elements and attributes """
    def populate_window(self):
        self.create_header()
        self.create_title()
        self.create_fields()
        self.create_button()

    ####################################### METHODS FOR WINDOW PROPERTIES #######################################

    """ Create a new header """
    def create_header(self):
        self.master.title('GUI example from T-Visor')
        self.pack(fill=BOTH, expand=1)

    """ Create a new title for the current window """
    def create_title(self):
        label = Label(self, text='Example prompt for the Database application', font=self.title_font)
        label.pack(side='top', fill='x', pady=10)

    """ Create fields for requesting user input """
    def create_fields(self):
        # label and field for the first name
        self.firstname_label = Label(self, text='First name')
        self.firstname_label.pack()
        self.firstname_entry = StringVar()
        self.firstname_field = Entry(self, textvariable=self.firstname_entry)
        self.firstname_field.pack()

        # label and field for the last name
        self.secondname_label = Label(self, text='Last name')
        self.secondname_label.pack()
        self.lastname_entry = StringVar()
        self.lastname_field = Entry(self, textvariable=self.lastname_entry)
        self.lastname_field.pack()
    
    """ Create a new button labeled 'ENTER' which will insert a new entry in the database
        and clear the entry fields """
    def create_button(self):
        enter_button = Button(self, text='ENTER', command=lambda:[self.enter_data(), self.clear_fields()])
        enter_button.place(relx=0.5, rely=0.5, anchor=CENTER)

    ######################################### METHODS TO HANDLE EVENTS ############################################

    """ Inserts the values entered from the 'First name' & 'Last name' fields """
    def enter_data(self):
        connection = sqlite3.connect('student.db')
        cursor = connection.cursor()
        cursor.execute('INSERT INTO stud (firstname, secondname) VALUES (?,?)', (self.firstname_entry.get(), self.lastname_entry.get()))
        connection.commit()
        print('Values successfully entered!')

    """ Clears the 'First name' & 'Last name' fields """
    def clear_fields(self):
        self.firstname_field.delete(0, 'end')
        self.lastname_field.delete(0, 'end')