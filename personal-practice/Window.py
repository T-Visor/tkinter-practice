import tkinter as tkinter
from tkinter import *
from tkinter.font import Font

""" This class creates a GUI window for an application (inherits from Frame) """
class Window(Frame):

    """ Initialize the new window """
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.master = master  
        self.title_font = Font(family='Helvetica', size=20, weight="bold", slant="italic")       
        self.populate_window()

    """ Populate the window with some elements and attributes """
    def populate_window(self):
        self.create_header()
        self.create_title()
        self.create_button()
        self.create_menu()
        self.show_text()

    """ Create a new header """
    def create_header(self):
        self.master.title('GUI example from T-Visor')
        self.pack(fill=BOTH, expand=1)

    def create_title(self):
        label = Label(self, text="Welcome to the first page!", font=self.title_font)
        label.pack(side="top", fill="x", pady=10)
    
    """ Create a new button labeled 'QUIT' which will exit the process upon being clicked
        and place it in the center """
    def create_button(self):
        quitButton = Button(self, text='QUIT', command=self.client_exit)
        quitButton.place(relx=0.5, rely=0.5, anchor=CENTER)

    """ Create a menu of options which will be displayed in the upper-left corner 
        (similar to a webpage) """
    def create_menu(self):
        menu = Menu(self.master)
        self.master.config(menu=menu)

        options = Menu(menu)
        options.add_command(label="Exit", command=self.client_exit)
        menu.add_cascade(label='Options', menu=options)

    """ Create a label which will display some text output"""
    def show_text(self):
        text = Label(self, text="Hey there good lookin! \n I think there \n is \n a lot \n which \n is \n going \n on \n")
        text.pack()

    """ Exit the current process """
    def client_exit(self):
        exit()